package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public Calculadora() {
    }

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public double multiplicacao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public int multiplicacaoNegativo(int primeiroNumero, int segundoNumero){
        if (primeiroNumero < 0 & segundoNumero < 0) {
            int resultado = primeiroNumero * segundoNumero;
            return resultado;
        }
        throw new RuntimeException("Os numeros precisam ser negativos");
    }

    public double divisao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public int divisao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public int divisaoNegativo(int primeiroNumero, int segundoNumero){
        if (primeiroNumero < 0 & segundoNumero < 0) {
            int resultado = primeiroNumero / segundoNumero;
            return resultado;
        }
        throw new RuntimeException("Os numeros precisam ser negativos");
    }
}

