package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        Calculadora calculadora = new  Calculadora();
        int resultado = calculadora.soma(1,2);

        Assertions.assertEquals(3,resultado);
    }

    @Test
    public void TestaASomaDeDoisNumerosNaoNaturais(){
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7,resultado);
    }

    @Test
    public void TestaMultiplicacaoNumerosFlutuantes(){
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.multiplicacao(1.2, 1.2);

        Assertions.assertEquals(1.44, resultado);
    }

    @Test
    public void TestaMultiplicacaoNumerosInteiros(){
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.multiplicacao(1, 5);

        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void TestaMultiplicacaoNumerosNegativos(){
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.multiplicacaoNegativo(-1, -5);

        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void TestaDivisaoNumerosFlutuantes(){
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.divisao(1.2, 1.2);

        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void TestaDivisaoNumerosInteiros(){
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.divisao(4, 2);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void TestaDivisaoNumerosNegativos(){
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.divisaoNegativo(1, -5);

        Assertions.assertEquals(1, resultado);
    }

}
